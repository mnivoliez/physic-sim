use std::ops::{Add, Mul, Sub};

use point::Point;

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct Vector(pub f32, pub f32, pub f32);

impl Vector {
    pub fn from_two_points(from: &Point, to: &Point) -> Vector {
        Vector(to.0 - from.0, to.1 - from.1, to.2 - from.2)
    }

    pub fn get_norme(&self) -> f32 {
        (self.0.powi(2) + self.1.powi(2) + self.2.powi(2)).sqrt()
    }

    fn normalize(&self) -> Vector {
        let norm = self.get_norme();
        Vector(self.0 / norm, self.1 / norm, self.2 / norm)
    }
}

impl Add for Vector {
    type Output = Vector;

    fn add(self, v: Vector) -> Vector {
        Vector(self.0 + v.0, self.1 + v.1, self.2 + v.2)
    }
}

impl Sub for Vector {
    type Output = Vector;

    fn sub(self, v: Self) -> Vector {
        Vector(self.0 - v.0, self.1 - v.1, self.2 - v.2)
    }
}

impl Mul<Vector> for f32 {
    type Output = Vector;

    fn mul(self, v: Vector) -> Vector {
        Vector(v.0 * self, v.1 * self, v.2 * self)
    }
}

impl Mul<Vector> for Vector {
    type Output = f32;

    fn mul(self, v: Vector) -> f32 {
        self.0 * v.0 + self.1 * v.1 + self.2 * v.2
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct NormalizedVector(pub Vector);

impl NormalizedVector {
    pub fn new(v: &Vector) -> NormalizedVector {
        NormalizedVector(v.normalize())
    }
}

impl Sub for NormalizedVector {
    type Output = Vector;

    fn sub(self, n: NormalizedVector) -> Vector {
        self.0 - n.0
    }
}

impl Mul for NormalizedVector {
    type Output = f32;

    fn mul(self, nv: NormalizedVector) -> f32 {
        self.0 * nv.0
    }
}

impl Mul<Vector> for NormalizedVector {
    type Output = f32;

    fn mul(self, v: Vector) -> f32 {
        self.0 * v
    }
}

impl Mul<NormalizedVector> for f32 {
    type Output = Vector;

    fn mul(self, nv: NormalizedVector) -> Vector {
        self * nv.0
    }
}

impl Mul<NormalizedVector> for Vector {
    type Output = f32;

    fn mul(self, nv: NormalizedVector) -> f32 {
        self * nv.0
    }
}
#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_from_two_point() {
        let (from, to) = (Point(1.0, 1.0, 1.0), Point(6.0, 6.0, 6.0));
        let v = Vector::from_two_points(&from, &to);
        assert_eq!(v, Vector(5.0, 5.0, 5.0));
    }

    #[test]
    fn test_get_norme() {
        let v = Vector(1.0, 2.0, 3.0);
        let norm = v.get_norme();
        let result = 14.0_f32.sqrt();
        assert_eq!(norm, result);
    }

    #[test]
    fn test_add() {
        let (v1, v2) = (Vector(1.0, 1.0, 2.0), Vector(2.0, 2.0, 1.0));
        let result = v1 + v2;
        assert_eq!(result, Vector(3.0, 3.0, 3.0));
    }

    #[test]
    fn test_mul_scalar() {
        let v = Vector(1.0, 1.0, 1.0);
        assert_eq!(2.0 * v, Vector(2.0, 2.0, 2.0));
    }

    #[test]
    fn test_dot_product() {
        let (v1, v2) = (Vector(2.0, 3.0, 4.0), Vector(5.0, 6.0, 7.0));
        let result = v1 * v2;
        assert_eq!(result, 56.0);
        let result2 = v2 * v1;
        assert_eq!(result, result2);
    }

    #[test]
    fn test_dot_product_normalized_vector() {
        let (v1, v2) = (
            NormalizedVector::new(&Vector(2.0, 3.0, 4.0)),
            Vector(5.0, 6.0, 7.0),
        );
        assert_eq!(v1 * v2, v2 * v1);
        assert_eq!(
            v1 * v2,
            5.0 * (2.0 / 29.0_f32.sqrt()) + 6.0 * (3.0 / 29.0_f32.sqrt()) +
                7.0 * (4.0 / 29.0_f32.sqrt())
        );
    }

    #[test]
    fn test_normalize() {
        let v = Vector(2.0, 3.0, 4.0);
        let result = v.normalize();
        assert_eq!(
            result,
            Vector(
                2.0 / 29.0_f32.sqrt(),
                3.0 / 29.0_f32.sqrt(),
                4.0 / 29.0_f32.sqrt()
            )
        );
    }
}
