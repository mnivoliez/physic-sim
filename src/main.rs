use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;

mod point;
mod vector;

use point::Point;
use vector::{NormalizedVector, Vector};

#[derive(Debug, Clone, Copy)]
struct Position(pub Point);
#[derive(Debug, Clone, Copy, PartialEq)]
struct Speed {
    pub speed: Vector,
    pub normalized_direction: NormalizedVector,
}
#[derive(Debug, Clone, Copy, PartialEq)]
struct Acceleration(pub Vector);

#[derive(Debug, Clone, Copy)]
struct Skydiver {
    pub position: Position,
    pub speed: Speed,
    pub masse: f32,
    pub c: f32,
}

impl Skydiver {
    pub fn to_string(&self) -> String {
        let p = self.position.0;
        let s = self.speed.speed;
        format!("{};{};{};{};", p.0, p.2, s.0, s.2)
    }
}

fn calculate_acceleration(c: f32, s: &Speed, m: f32) -> Acceleration {
    assert!(!c.is_nan());
    assert!(!m.is_nan());
    let cnormonm = (c * s.speed.get_norme().powi(2)) / m;
    Acceleration(Vector(0.0, 0.0, -9.81) - (cnormonm * s.normalized_direction))
}

fn calculate_speed(s: &Speed, a: &Acceleration, delta_t: f32) -> Speed {
    let speed = s.speed + delta_t * a.0;
    Speed {
        speed: speed,
        normalized_direction: NormalizedVector::new(&speed),
    }
}

fn calculate_position(p: &Position, s: &Speed, delta_t: f32) -> Position {
    Position(p.0 + delta_t * s.speed)
}

fn main() {
    let mut sds = vec![
        Skydiver {
            position: Position(Point(0.0, 0.0, 4000.0)),
            speed: Speed {
                speed: Vector(41.6, 0.0, 0.0),
                normalized_direction: NormalizedVector::new(&Vector(41.6, 0.0, 0.0)),
            },
            masse: 60.0,
            c: 0.33,
        },
        Skydiver {
            position: Position(Point(200.0, 0.0, 4000.0)),
            speed: Speed {
                speed: Vector(41.6, 0.0, 0.0),
                normalized_direction: NormalizedVector::new(&Vector(41.6, 0.0, 0.0)),
            },
            masse: 85.0,
            c: 0.33,
        },
        Skydiver {
            position: Position(Point(400.0, 0.0, 4000.0)),
            speed: Speed {
                speed: Vector(41.6, 0.0, 0.0),
                normalized_direction: NormalizedVector::new(&Vector(41.6, 0.0, 0.0)),
            },
            masse: 120.0,
            c: 0.33,
        },
    ];

    let mut out: String = String::from("TIME; ;");
    for (id, _) in sds.iter().enumerate() {
        out += &format!("sd{id} px; sd{id} pz{id}; sd{id} sx; sd{id} sz; ;", id = id);
    }
    out += &format!("\n");
    let mut t = 0.0;
    let delta_t = 0.001;
    out += &format!("{}; ;", t);
    for sd in sds.iter() {
        out += &sd.to_string();
        out += &format!(";");
    }
    out += &format!("\n");
    while t < 70.0 {
        out += &format!("{}; ;", t);
        for mut sd in sds.iter_mut() {
            let a = calculate_acceleration(sd.c, &sd.speed, sd.masse);
            sd.speed = calculate_speed(&sd.speed, &a, delta_t);
            sd.position = calculate_position(&sd.position, &sd.speed, delta_t);
            out += &sd.to_string();
            out += &format!(";");
        }
        out += &format!("\n");
        t += delta_t;
    }
    let path = Path::new("simu.csv");
    let display = path.display();

    // Open a file in write-only mode, returns `io::Result<File>`
    let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}", display, why.description()),
        Ok(file) => file,
    };

    match file.write_all(out.as_bytes()) {
        Err(why) => panic!("couldn't write to {}: {}", display, why.description()),
        Ok(_) => println!("successfully wrote to {}", display),
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_calculate_acceleration() {
        let v = Vector(1.0, 2.0, 3.0);
        let nv = NormalizedVector::new(&v);
        let s = Speed {
            speed: v,
            normalized_direction: NormalizedVector::new(&v),
        };
        let (c, m) = (1.0, 10.0);
        let velo = calculate_acceleration(c, &s, m);
        assert_eq!(
            velo,
            Acceleration(Vector(0.0, 0.0, -9.81) - (v.get_norme().powi(2) / m) * nv)
        );
    }

    #[test]
    fn test_calculate_speed() {
        let s0 = Speed {
            speed: Vector(0.0, 0.0, 1.0),
            normalized_direction: NormalizedVector::new(&Vector(0.0, 0.0, 1.0)),
        };
        let a = Acceleration(Vector(1.0, 1.0, 1.0));
        let s1 = calculate_speed(&s0, &a, 0.5);
        assert_eq!(
            s1,
            Speed {
                speed: Vector(0.5, 0.5, 1.5),
                normalized_direction: NormalizedVector::new(&Vector(0.5, 0.5, 1.5)),
            }
        );
    }
}
