use std::ops::Add;
use vector::Vector;

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct Point(pub f32, pub f32, pub f32);

impl Add<Vector> for Point {
    type Output = Point;

    fn add(self, v: Vector) -> Point {
        Point(v.0 + self.0, v.1 + self.1, v.2 + self.2)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_translate() {
        let (p, v) = (Point(3.0, 2.0, 1.0), Vector(5.0, 5.0, 5.0));
        assert_eq!(p + v, Point(8.0, 7.0, 6.0));
    }
}
